---
title: Compose Graphics 1
template: main-full.html
---

## Introduction

Building custom components with Jetpack Compose is significantly easier than it used to be with the old View approach!

In this module, we'll take a quick look at the old way and compare it to Compose, then dive in for some progressively more complex custom components, drawing everything ourselves to create exactly the UI we would like.

## Video

Total video time for this module: 1:47:55

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 

    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

### Jetpack Compose Graphics: Introduction (Fall 2021) (06:50)

<iframe width="800" height="450" src="https://www.youtube.com/embed/1t41a-X8Ewc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: A Simple Gauge (Fall 2021) (24:32)

<iframe width="800" height="450" src="https://www.youtube.com/embed/BpVQus2r4qs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: An Analog Clock (Fall 2021) (20:00)

<iframe width="800" height="450" src="https://www.youtube.com/embed/5FtqH8dv0BM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: Graph Editor, Part 1 (Fall 2021) (56:33)

<iframe width="800" height="450" src="https://www.youtube.com/embed/kNLr8Oe8vnA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/compose-graphics-1](https://gitlab.com/android-development-2022-refresh/compose-graphics-1)
